# Changelog

## ver 0.6.2 (2020-09-15)

- fixed position updating
- minor bugfixes
- spacebar to pause in gui

## ver 0.6.1 (2020-09-02)

- changes in the look of the command interface
- minor bugfixes

## ver 0.6.0 (2020-08-29)

- Fixed possible multiple daemons running together.
- Modified files first line (#!/usr/bin/env python)
- "version" command added
- "restart" command works also in stop state
- gui insert position accept things like "1:23" (not only "01:23")
- gui exits at the end of files
- daemon doesn't exit on stop
- item data in .cache deleted after reaching the end of the file
- "gui" command now has same behaviour of "play -g"

## ver 0.5.0 (2020-08-23)

- Initial public version. Command line and gui ready!

